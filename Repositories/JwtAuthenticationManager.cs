using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CIIForumAPI.DAL;
using CIIForumAPI.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CIIForumAPI.Repositories
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IConfiguration configuration;

        public JwtAuthenticationManager(ApplicationDbContext dbContext , IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
        }
        
        /// <summary>
        /// Authenticates User and send return token for Authentic users
        /// </summary>
        /// <param name="cred"></param>
        /// <returns>Auth</returns>
        public async Task<Auth> Authenticate(Cred cred)
        {
            Auth auth = new Auth();
            var user = await dbContext.User.FirstOrDefaultAsync(person => person.Username == cred.username);
            if(user == null)
            {
                auth.IsUsernameValid = false;
                auth.IsPasswordValid = null;
                auth.Token = null;
                return auth;
            }
            else 
            {
                if(user.Username == cred.username)
                {
                    if(user.Password == cred.password)
                    {
                        var role = await dbContext.Role.FirstOrDefaultAsync(roles=> roles.RoleId == user.RoleId);
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var tokenKey = Encoding.ASCII.GetBytes(configuration["Jwt:Key"]);
                        var tokenDescriptor = new SecurityTokenDescriptor{
                            Subject = new ClaimsIdentity(new Claim[]{
                                new Claim("username" , cred.username),
                                new Claim(ClaimTypes.Role , role.RoleName)
                            }),
                            SigningCredentials = new SigningCredentials(
                                new SymmetricSecurityKey(tokenKey),
                                SecurityAlgorithms.HmacSha256Signature
                            )
                        };
                        var token = tokenHandler.CreateToken(tokenDescriptor);
                        auth.Token = tokenHandler.WriteToken(token);
                        auth.IsUsernameValid = true;
                        auth.IsPasswordValid = true;
                        return auth;
                    }
                    else
                    {
                        auth.IsUsernameValid = true;
                        auth.IsPasswordValid = false;
                        auth.Token = null;
                        return auth;
                    }
                }
                else
                {    
                    auth.IsUsernameValid = false;
                    auth.IsPasswordValid = null;
                    auth.Token = null;
                    return auth;
                }
            }
        }
    }
}