using System.Diagnostics.CodeAnalysis;
using CIIForumAPI.DAL;
using Microsoft.EntityFrameworkCore;

namespace CIIForumAPI.Repositories
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions options):base(options){}
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<PostDb> Post { get; set; }
        public DbSet<PostWithAvatar> Posts { get; set; }
        public DbSet<CommentWithAvatar> Comments { get; set; }
        public DbSet<LikeDb> Like { get; set; }
        public DbSet<CommentDb> Comment { get; set; }
    }
}
