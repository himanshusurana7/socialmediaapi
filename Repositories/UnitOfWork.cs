using System.Threading.Tasks;
using CIIForumAPI.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CIIForumAPI.Repositories
{
    public class UnitOfWork : IUnitofWork
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IConfiguration configuration;

        public UnitOfWork(ApplicationDbContext dbContext , IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
        }
        public IUserRepository userRepository => new UserRepository(dbContext);

        public IJwtAuthenticationManager authenticationManager => new JwtAuthenticationManager(dbContext , configuration );

        public IPostRepository postRepository => new PostRepository(dbContext);

        public async Task Save()
        {
            await dbContext.SaveChangesAsync();
        }
    }
}