using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CIIForumAPI.API.Post;
using CIIForumAPI.DAL;
using CIIForumAPI.Interfaces;
using Microsoft.EntityFrameworkCore;
using Post = CIIForumAPI.DAL.Post;

namespace CIIForumAPI.Repositories
{
    public class PostRepository : IPostRepository
    {
        #region SQL Queries
        private static readonly string _getTenPosts = @"SELECT post.PostId,u.Username,post.Text,post.Image,post.PostDate,u.Picture as Avatar FROM [CIIForum].[dbo].[Post] post LEFT JOIN [CIIForum].[dbo].[User] u ON post.UserId = u.UserId Where post.PostId between {0} and {1} ORDER BY Post.PostID Desc;";
        private static readonly string _getAllPosts = @"SELECT post.PostId,u.Username,post.Text,post.Image,post.PostDate,u.Picture as Avatar FROM [CIIForum].[dbo].[Post] post LEFT JOIN [CIIForum].[dbo].[User] u ON post.UserId = u.UserId ORDER BY Post.PostID Desc;";
        private static readonly string _getAllPostsByUsername = @"SELECT post.PostId,u.Username,post.Text,post.Image,post.PostDate,u.Picture as Avatar FROM [CIIForum].[dbo].[Post] post LEFT JOIN [CIIForum].[dbo].[User] u ON post.UserId= u.UserId WHERE u.Username = {0} ORDER BY Post.PostID Desc;";
        private static readonly string _getCommentsByPostId = @"SELECT c.CommentId ,u.Username ,c.PostId ,c.Text ,c.CommentDate, u.Picture as Avatar FROM [CIIForum].[dbo].[Comment] c LEFT JOIN [CIIForum].[dbo].[User] u ON c.UserId = u.UserId WHERE c.PostId = {0};";        
        #endregion
        private readonly ApplicationDbContext dbContext;

        public PostRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        #region Public methods

        /// <summary>
        /// returns all the posts with likes and comments.
        /// </summary>
        /// <returns>PostResponse</returns>
        public async Task<PostResponse> GetAllPosts()
        {
            PostResponse response = new PostResponse();
            var postsQuery = dbContext.Posts.FromSqlRaw(_getAllPosts).ToList();
            List<API.Post.Post> responsePosts = new List<API.Post.Post>();
            if (postsQuery != null)
            {
                foreach (var post in postsQuery)
                {
                    API.Post.Post responsePost = new API.Post.Post();

                    responsePost.PostId = post.PostId;
                    responsePost.Text = post.Text;
                    responsePost.Image = post.Image;
                    responsePost.Username = post.Username;
                    responsePost.PostDate = post.PostDate;
                    responsePost.likes = await this.CountLikes(post.PostId);
                    responsePost.Avatar = post.Avatar;

                    responsePosts.Add(responsePost);
                }
            }
            response.posts = responsePosts;
            return response;
        }

        /// <summary>
        /// Returns a list of posts made by the given user.
        /// </summary>
        /// <param name="Username"></param>
        /// <returns></returns>
        public async Task<PostResponse> GetPostByUsername(string Username)
        {
            var posts = dbContext.Posts.FromSqlRaw(_getAllPostsByUsername,Username).ToList();
            PostResponse response = new PostResponse();
            List<API.Post.Post> responsePosts = new List<API.Post.Post>();

            if (posts != null)
            {
                foreach (var post in posts)
                {
                    API.Post.Post responsePost = new API.Post.Post();

                    responsePost.PostId = post.PostId;
                    responsePost.Text = post.Text;
                    responsePost.Image = post.Image;
                    responsePost.Username = post.Username;
                    responsePost.PostDate = post.PostDate;
                    responsePost.likes = await this.CountLikes(post.PostId);
                    responsePost.Avatar = post.Avatar;

                    responsePosts.Add(responsePost);
                }
            }
            response.posts = responsePosts;
            return response;
        }

        /// <summary>
        /// Validates the data of post and add it to exisiting posts.
        /// Returns true or false based on the validation status of post.
        /// </summary>
        /// <param name="post"></param>
        /// <returns>bool</returns>
        public async Task<bool> AddPost(Post post)
        {
            if (checkPostContent(post))
            {
                User user = await dbContext.User.FirstOrDefaultAsync(u => u.Username == post.Username);
                PostDb newPost = new PostDb();
                newPost.PostId = post.PostId;
                newPost.PostDate = DateTime.Today;
                newPost.Image = post.Image;
                newPost.Text = post.Text;
                newPost.UserId = user.UserId;
                await dbContext.Post.AddAsync(newPost);
                return true;
            }
            else return false;
        }
        /// <summary>
        /// Get the PostId of the last post added. 
        /// </summary>
        /// <returns>int PostId</returns>
        public int GetLastPostId()
        {
            return dbContext.Post.Max(post=> post.PostId);   
        }
        /// <summary>
        /// Finds all the posts between given postIds(startingId and endingId)
        /// </summary>
        /// <param name="startingId"></param>
        /// <param name="endingId"></param>
        /// <returns>PostResponse</returns>
        public async Task<PostResponse> GetAllPostBetweenIds(int startingId , int endingId)
        {   
            PostResponse response = new PostResponse();
            if(startingId<=1)
                response.isLastPost = true;
            var postsQuery = dbContext.Posts.FromSqlRaw(_getTenPosts,startingId , endingId).ToList();
            List<API.Post.Post> responsePosts = new List<API.Post.Post>();
            if (postsQuery != null)
            {
                foreach (var post in postsQuery)
                {
                    API.Post.Post responsePost = new API.Post.Post();

                    responsePost.PostId = post.PostId;
                    responsePost.Text = post.Text;
                    responsePost.Image = post.Image;
                    responsePost.Username = post.Username;
                    responsePost.PostDate = post.PostDate;
                    responsePost.likes = await this.CountLikes(post.PostId);
                    responsePost.Avatar = post.Avatar;

                    responsePosts.Add(responsePost);
                }
            }
            response.posts = responsePosts;
            return response;
        }
        /// <summary>
        /// Removes the post with the given PostId
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>bool</returns>
        public async Task<bool> RemovePost(int postId)
        {
            PostDb postToBeDeleted = await dbContext.Post.FirstOrDefaultAsync(post => post.PostId == postId);
            if (postToBeDeleted != null)
            {
                dbContext.Post.Remove(postToBeDeleted);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Update the post with the given PostId 
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="text"></param>
        /// <returns>bool</returns>
        public async Task<bool> UpdatePost(int postId, string text)
        {
            PostDb postToBeUpdated = await dbContext.Post.FirstOrDefaultAsync(post => post.PostId == postId);
            if (postToBeUpdated != null)
            {
                postToBeUpdated.Text = text;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Likes or unlikes a post based on given likeData
        /// </summary>
        /// <param name="likeData"></param>
        /// <returns>bool</returns>
        public async Task<bool> LikeOrUnlike(Like likeData)
        {
            User user = await dbContext.User.FirstOrDefaultAsync(u => u.Username == likeData.Username);
            var alredyLiked = await dbContext.Like.FirstOrDefaultAsync(like => like.PostId == likeData.PostId && like.UserId == user.UserId);
            if (alredyLiked != null)
            {
                dbContext.Like.Remove(alredyLiked);
                return false;
            }
            else
            {
                LikeDb newLikeData = new LikeDb();   
                newLikeData.UserId = user.UserId;
                newLikeData.PostId = likeData.PostId;
                newLikeData.LikeId = likeData.LikeId;
                dbContext.Like.Add(newLikeData);
                return true;
            }
        }
        /// <summary>
        /// Add Comments to given post based on Comment data
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task AddComment(DAL.Comment comment)
        {
            User user = await dbContext.User.FirstOrDefaultAsync(u => u.Username == comment.Username);
            CommentDb newComment = new CommentDb();
            newComment.CommentId = comment.CommentId;
            newComment.PostId = comment.PostId;
            newComment.Text = comment.Text;
            newComment.UserId = user.UserId;
            newComment.CommentDate = DateTime.Today;
            await dbContext.Comment.AddAsync(newComment);
        }
        /// <summary>
        /// Deletes a given comment Id
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public async Task DeleteComment(int commentId)
        {
            var commentToBeDeleted = await dbContext.Comment.FirstOrDefaultAsync(comment => comment.CommentId == commentId);
            dbContext.Comment.Remove(commentToBeDeleted);
        }
        /// <summary>
        /// Updates a comment with the given text.
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="updatedCommentText"></param>
        /// <returns></returns>
        public async Task UpdateComment(int commentId, string updatedCommentText)
        {
            var commentToBeUpdated = await dbContext.Comment.FirstOrDefaultAsync(comment => comment.CommentId == commentId);
            commentToBeUpdated.Text = updatedCommentText;
        }
        /// <summary>
        /// returns the PostId of the latest post made by the given user
        /// </summary>
        /// <param name="username"></param>
        /// <returns>int</returns>
        public async Task<int> GetLastestPostByUsername(string username)
        {
            User user = await dbContext.User.FirstOrDefaultAsync(u => u.Username == username);
            PostDb latestPost = await dbContext.Post.OrderBy(post=> post.PostId).LastOrDefaultAsync(post => post.UserId == user.UserId);
            return latestPost.PostId;
        }
        /// <summary>
        /// returns the CommentId of the latest comment made by the user on a given postId
        /// </summary>
        /// <param name="username"></param>
        /// <param name="postId"></param>
        /// <returns>int</returns>
        public async Task<int> GetLatestCommentByUsernameAndPostId(string username , int postId)
        {
            User user = await dbContext.User.FirstOrDefaultAsync(u => u.Username == username);
            var latestComment = await dbContext.Comment.OrderBy(comment=>comment.CommentId).LastOrDefaultAsync(comment=> comment.PostId == postId && comment.UserId == user.UserId); 
            return latestComment.CommentId;
        }
        /// <summary>
        /// Retrurns all the comments on a given Post.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public CommentResponse GetCommentByPostId(int postId)
        {
            CommentResponse response = new CommentResponse();
            var comments = dbContext.Comments.FromSqlRaw(_getCommentsByPostId,postId).ToList();
            List<API.Post.Comment> commentsWithAvatar = new List<API.Post.Comment>();
            if (comments.Count > 0)
            {
                foreach(var comment in comments)
                {
                    API.Post.Comment commentWithAvatar = new API.Post.Comment();
                    
                    commentWithAvatar.PostId = comment.PostId;
                    commentWithAvatar.CommentDate = comment.CommentDate;
                    commentWithAvatar.CommentId = comment.CommentId;
                    commentWithAvatar.Avatar =  comment.Avatar;
                    commentWithAvatar.Text = comment.Text;
                    commentWithAvatar.Username = comment.Username; 
                    
                    commentsWithAvatar.Add(commentWithAvatar);
                }
            }    
            response.comments = commentsWithAvatar;            
            return response;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Returns true if the Post content is valid else returns false.
        /// </summary>
        /// <param name="post"></param>
        /// <returns>bool</returns>
        private bool checkPostContent(Post post)
        {
            if ((post.Text == null || post.Text == "") && post.Image == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the number of likes on a Post
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>int</returns>
        private async Task<int> CountLikes(int postId)
        {
            return await dbContext.Like.CountAsync(like => like.PostId == postId);
        }

        #endregion
    }
}