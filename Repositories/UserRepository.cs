using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CIIForumAPI.API.User;
using CIIForumAPI.DAL;
using CIIForumAPI.Interfaces;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using User = CIIForumAPI.DAL.User;

namespace CIIForumAPI.Repositories
{
    public class UserRepository:IUserRepository
    {
        private readonly ApplicationDbContext dbContext;

        public UserRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        #region Public Methods
        /// <summary>
        /// Adds a given user to the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task AddUser(User user)
        {
            //TODO: Change this RoleId for the admin settings.
            user.RoleId = 2;
            if(await IsUsernameAvailable(user.Username))
            {
                await dbContext.User.AddAsync(user);
            }        
        }
        /// <summary>
        /// Retrives all users information
        /// </summary>
        /// <returns></returns>
        public async Task<AllUsersResponse> GetAllUsers()
        {
            AllUsersResponse response = new AllUsersResponse();
            List<User> users = await dbContext.User.ToListAsync();
            List<API.User.User> resposeUsers = new List<API.User.User>();
            foreach (var user in users)
            {
                API.User.User responseUser = new API.User.User();
                responseUser.UserId = user.UserId;
                responseUser.Dob = user.Dob != null ? Convert.ToDateTime(user.Dob).Date.ToString("yyyy-m-dd"):null;
                responseUser.Email = user.Email;
                responseUser.Name = user.Name;
                responseUser.Phone = user.Phone;
                responseUser.Picture = user.Picture;
                responseUser.RoleId = user.RoleId;
                responseUser.Username = user.Username; 

                resposeUsers.Add(responseUser);
            }  
            response.users = resposeUsers;
            return response; 
        }
        /// <summary>
        /// Retrives a pirticualr user according to the given username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<UserResponse> GetUserByUsername(string username)
        {
            UserResponse response = new UserResponse();
            User user = await dbContext.User.SingleOrDefaultAsync(user => user.Username == username);
            API.User.User responseUser = new API.User.User();
            responseUser.UserId = user.UserId;
            responseUser.Dob = user.Dob != null? Convert.ToDateTime(user.Dob).ToString("yyyy-MM-dd") : null;
            responseUser.Email = user.Email;
            responseUser.Name = user.Name;
            responseUser.Phone = user.Phone;
            responseUser.Picture = user.Picture;
            responseUser.RoleId = user.RoleId;
            responseUser.Username = user.Username;

            response.user = responseUser; 
            return response;
        }
        /// <summary>
        /// Removes the given user from the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task RemoveUser(string username)
        {
            var user = await dbContext.User.FirstOrDefaultAsync(registeredUser => registeredUser.Username == username);
            if( user != null)
            {
                dbContext.User.Remove(user);
            }      
        }
        /// <summary>
        /// Updates the User data
        /// </summary>
        /// <param name="username"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task UpdateUser(string username, JsonPatchDocument user)
        {
            var userWithOldDetails = await dbContext.User.FirstOrDefaultAsync(user => user.Username == username);
            user.ApplyTo(userWithOldDetails);
        }
        /// <summary>
        /// Checks is the given username is already taken or not
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<bool> IsUsernameAvailable(string username)
        {
            var registeredUser = await dbContext.User.FirstOrDefaultAsync(registeredUser => registeredUser.Username == username);
            if(registeredUser == null)
            {
                return true;
            }
            else if(!registeredUser.Username.Equals(username))
                return true;
            else return false;
        }

        public async Task<bool> ChangePassword(string username , ChangePassword request)
        {
            var registeredUser = await dbContext.User.FirstOrDefaultAsync(registeredUser => registeredUser.Username == username);
            if(registeredUser != null && registeredUser.Password == request.oldPassword)
            {
                registeredUser.Password = request.newPassword;
                return true;
            }
            return false;
        }
        #endregion
    }
}