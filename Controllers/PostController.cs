using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CIIForumAPI.API.Post;
using CIIForumAPI.DAL;
using CIIForumAPI.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Post = CIIForumAPI.DAL.Post;

namespace CIIForumAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("Post")]
    public class PostController : ControllerBase
    {
        private readonly IUnitofWork unitofWork;

        public PostController(IUnitofWork unitofWork)
        {
            this.unitofWork = unitofWork;
        }

        #region Get Methods
        
        [HttpGet("Initial")]
        public async Task<ActionResult<PostResponse>> GetInitialPost()
        {
            try
            {
                int endingId = unitofWork.postRepository.GetLastPostId();
                return await unitofWork.postRepository.GetAllPostBetweenIds(endingId-9 , endingId); 
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }
        [HttpGet("Ending/{endingId:int}")]
        public async Task<ActionResult<PostResponse>> GetNextPosts([FromRoute] int endingId)
        {
            try
            {
                return await unitofWork.postRepository.GetAllPostBetweenIds(endingId-9 , endingId);
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<PostResponse>> GetPostsByUsername([FromRoute] string username)
        {
            try
            {
                  return await unitofWork.postRepository.GetPostByUsername(username);
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpGet("Comment/{postId}")]
        public ActionResult<CommentResponse> GetCommentByPostId([FromRoute] int postId){
            try
            {
                return unitofWork.postRepository.GetCommentByPostId(postId);
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        #endregion
        
        #region Post Methods
        [HttpPost]
        public async Task<ActionResult> AddPost([FromBody] Post post)
        {
            try
            {
                bool isPostAdded = await unitofWork.postRepository.AddPost(post);
                if(isPostAdded)
                {
                    await unitofWork.Save();    
                    return Ok(await unitofWork.postRepository.GetLastestPostByUsername(post.Username));
                }
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpPost("Update/{id}")]
        public async Task<ActionResult> UpdatePost([FromRoute] int postId , [FromBody] string text)
        {
            try
            {
                bool isPostUpdated = await unitofWork.postRepository.UpdatePost(postId,text);
                if(isPostUpdated)
                {
                    await unitofWork.Save();    
                    return Ok();
                }
                else
                    return BadRequest();
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }    
        }

        [HttpPost("Like")]
        public async Task<ActionResult> LikeOrUnlike([FromBody] Like like)
        {
            try
            {
                bool isLiked = await unitofWork.postRepository.LikeOrUnlike(like);
                await unitofWork.Save();
                return Ok(isLiked); 
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpPost("Comment")]
        public async Task<ActionResult> AddComment([FromBody] DAL.Comment comment)
        {
            try
            {
                await unitofWork.postRepository.AddComment(comment);
                await unitofWork.Save();
                return Ok(await unitofWork.postRepository.GetLatestCommentByUsernameAndPostId(comment.Username,comment.PostId));
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        #endregion

        #region Delete Methods
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePost([FromRoute] int postId)
        {
            try
            {
                bool isPostDeleted = await unitofWork.postRepository.RemovePost(postId);
                if(isPostDeleted)
                {
                    await unitofWork.Save();
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        #endregion
    }
}