﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CIIForumAPI.API.User;
using CIIForumAPI.DAL;
using CIIForumAPI.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using User = CIIForumAPI.DAL.User;

namespace CIIForumAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("User")]
    public class UserController : ControllerBase
    {
        private readonly IUnitofWork unitofWork;

        public UserController(IUnitofWork unitofWork)
        {
            this.unitofWork = unitofWork;
        }

        #region Get Methods

        [HttpGet]
        public async Task<ActionResult<AllUsersResponse>> GetUsers()
        {
            try
            {
                return await unitofWork.userRepository.GetAllUsers();
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<UserResponse>> GetUserByUsername([FromRoute] string username)
        {
            try
            {
                return await unitofWork.userRepository.GetUserByUsername(username);
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [AllowAnonymous]
        [HttpGet("username/{username}")]
        public async Task<bool> IsUsernameAvailable(string username)
        {
            return await unitofWork.userRepository.IsUsernameAvailable(username);
        }

        #endregion
        
        #region Post Methods
        [AllowAnonymous]
        [HttpPost]
        [Route("authenticate")]
        public async Task<ActionResult> Authenticate([FromBody]Cred cred)
        {
            var Authtoken = await unitofWork.authenticationManager.Authenticate(cred);
            return Ok(Authtoken); 
        }

        
        [AllowAnonymous]
        [HttpPost]
        public async Task AddUser([FromBody] User user)
        {
            await unitofWork.userRepository.AddUser(user);
            await unitofWork.Save();
        }

        [HttpPost("password/{username}")]
        public async Task<ActionResult> ChangePassword([FromRoute] string username ,[FromBody] ChangePassword request )
        {
            try
            {
                if(await unitofWork.userRepository.ChangePassword(username,request))
                {
                    await unitofWork.Save();
                    return Ok(true);
                }
                return Ok(false);
            }
            catch(Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        #endregion
        
        #region Other Methods
        [HttpDelete("{username}")]
        public async Task RemoveUser([FromRoute] string username)
        {
            await unitofWork.userRepository.RemoveUser(username);
            await unitofWork.Save();
        }

        [HttpPatch("{username}")]
        public async Task UpdateUser([FromRoute] string username , [FromBody] JsonPatchDocument user)
        {
            await unitofWork.userRepository.UpdateUser(username , user);
            await unitofWork.Save();
        }

        #endregion
    }
}
