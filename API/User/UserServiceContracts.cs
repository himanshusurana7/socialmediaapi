using System.Collections.Generic;

namespace CIIForumAPI.API.User
{
    public class UserResponse
    {
        public User user {get;set;}
    }

    public class AllUsersResponse
    {
        public List<User> users {get;set;}
    }

    public class ChangePasswordRequest
    {
        public ChangePassword passwords {get;set;}
    }
}