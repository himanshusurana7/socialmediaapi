using System;

namespace CIIForumAPI.API.User
{
    public class User
    {
        public int UserId {get;set;}
        public string Username {get;set;}
        public string Email {get;set;}
        public int RoleId {get;set;}
        public string Name {get;set;}
        public string Dob {get;set;}
        public string Phone {get;set;}
        public byte[] Picture{get;set;}    
    }

    public class ChangePassword
    {
        public string oldPassword {get;set;}
        public string newPassword {get;set;} 
    } 
}