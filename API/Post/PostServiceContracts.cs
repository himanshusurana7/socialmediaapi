using System.Collections.Generic;

namespace CIIForumAPI.API.Post
{
    public class PostResponse
    {
        public List<Post> posts {get;set;}
        public bool isLastPost {get;set;}
    }   

    public class CommentResponse
    {
        public List<Comment> comments{get;set;}
    }
}