using System;

namespace CIIForumAPI.API.Post
{
    public class Post
    {
        public int PostId { get; set; }
        public string Username { get; set; }
        public byte[] Avatar { get; set;}
        public string Text { get; set; }
        public byte[] Image { get; set; }
        public DateTime PostDate { get; set; }
        public int likes { get; set; }    
    }

    public class Comment
    {
        public int CommentId { get; set; }
        
        public string Username { get; set; }
        
        public int PostId { get; set; }

        public string Text { get; set; }

        public DateTime CommentDate { get; set; }

        public  byte[] Avatar { get; set; }

    } 
}