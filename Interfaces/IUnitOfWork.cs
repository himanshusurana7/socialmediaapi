using System.Threading.Tasks;

namespace CIIForumAPI.Interfaces
{
    public interface IUnitofWork
    {
        IJwtAuthenticationManager authenticationManager {get;}
        IUserRepository userRepository {get;}
        IPostRepository postRepository{get;}
        Task Save();
    }
}