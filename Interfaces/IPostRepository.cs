using System.Collections.Generic;
using System.Threading.Tasks;
using CIIForumAPI.API.Post;
using CIIForumAPI.DAL;
using Post = CIIForumAPI.DAL.Post;

namespace CIIForumAPI.Interfaces
{
    public interface IPostRepository
    {
        Task<PostResponse> GetAllPosts();
        Task<PostResponse> GetPostByUsername(string Username);
        Task<bool> AddPost(Post post);
        Task<bool> RemovePost(int postId);
        Task<bool> UpdatePost(int postId, string text);
        Task<bool> LikeOrUnlike(Like likeData);
        Task AddComment(DAL.Comment comment);
        Task DeleteComment(int commentId);
        Task UpdateComment(int commentId , string updatedCommentText);
        CommentResponse GetCommentByPostId(int postId);
        Task<int> GetLastestPostByUsername(string username);
        Task<int> GetLatestCommentByUsernameAndPostId(string username , int postId);
        int GetLastPostId();
        Task<PostResponse> GetAllPostBetweenIds(int startingId , int endingId);
    }
}