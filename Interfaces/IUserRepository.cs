using System.Collections.Generic;
using System.Threading.Tasks;
using CIIForumAPI.API.User;
using CIIForumAPI.DAL;
using Microsoft.AspNetCore.JsonPatch;
using User = CIIForumAPI.DAL.User;

namespace CIIForumAPI.Interfaces
{
    public interface IUserRepository
    {
        Task<AllUsersResponse> GetAllUsers();
        Task<UserResponse> GetUserByUsername(string username);
        Task AddUser(User user);
        Task RemoveUser(string username);
        Task UpdateUser(string username , JsonPatchDocument user);
        Task<bool> IsUsernameAvailable(string username);
        Task<bool> ChangePassword(string username , ChangePassword request);
    }
}