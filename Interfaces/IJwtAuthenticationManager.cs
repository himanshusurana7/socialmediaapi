using System.Threading.Tasks;
using CIIForumAPI.DAL;

namespace CIIForumAPI.Interfaces
{
    public interface IJwtAuthenticationManager
    {
        Task<Auth> Authenticate(Cred cred);
    }
}