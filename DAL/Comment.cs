using System;
using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        
        [Required]
        public string Username { get; set; }
        
        [Required]
        public int PostId { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime CommentDate { get; set; }
    } 

    public class CommentDb
    {
        [Key]
        public int CommentId { get; set; }
        
        [Required]
        public int UserId { get; set; }
        
        [Required]
        public int PostId { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime CommentDate { get; set; }

    } 
}