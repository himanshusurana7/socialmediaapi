using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class  Role
    {
        [Key]
        public int RoleId {get;set;}
        public string RoleName {get;set;}
    }
}