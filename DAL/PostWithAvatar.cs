using System;
using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class PostWithAvatar
    {
        [Key]
        public int PostId { get; set; }
        [Required]
        public string Username { get; set; }
        public string Text { get; set; }
        public byte[] Image { get; set; }
        public DateTime PostDate { get; set; }
        public   byte[] Avatar { get; set; }

    }
}