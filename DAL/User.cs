using System;
using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class User 
    {
        [Key]
        public int UserId {get;set;}
        
        [Required]
        //[RegularExpression("^[A-Za-z0-9_]*$")]
        public string Username {get;set;}
        
        [Required]
        [EmailAddress]
        public string Email {get;set;}
        
        [Required]
        [MinLength(8)]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=()-])(?=\\S+$).{8,}$")]
        public string Password {get;set;}

        [Required]
        public int RoleId {get;set;}
        
        [Required]
        public string Name {get;set;}

        public DateTime? Dob {get;set;}

        public string Phone {get;set;}

        public byte[] Picture{get;set;}
    }
}