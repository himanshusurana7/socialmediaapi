using System;
using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        [Required]
        public string Username { get; set; }
        public string Text { get; set; }
        public byte[] Image { get; set; }
        public DateTime PostDate { get; set;}

    }

    public class PostDb
    {
        [Key]
        public int PostId { get; set; }
        [Required]
        public int UserId { get; set; }
        public string Text { get; set; }
        public byte[] Image { get; set; }
        public DateTime PostDate { get; set;}
    }
}