namespace CIIForumAPI.DAL
{
    public class  Auth
    {
        public bool IsUsernameValid {get;set;}
        public bool? IsPasswordValid {get;set;}
        public string Token {get;set;}
    }
}