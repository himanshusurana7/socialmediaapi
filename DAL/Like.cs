using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class Like
    {
        [Key]
        public int LikeId {get;set;}
        
        [Required]
        public int PostId {get;set;}

        [Required]
        public string Username {get;set;} 
    }

    public class LikeDb
    {
        [Key]
        public int LikeId {get;set;}
        
        [Required]
        public int PostId {get;set;}

        [Required]
        public int UserId {get;set;} 
    }
}