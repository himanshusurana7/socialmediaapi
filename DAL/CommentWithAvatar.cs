using System;
using System.ComponentModel.DataAnnotations;

namespace CIIForumAPI.DAL
{
    public class CommentWithAvatar
    {
        [Key]
        public int CommentId { get; set; }
        
        [Required]
        public string Username { get; set; }
        
        [Required]
        public int PostId { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime CommentDate { get; set; }

        public  byte[] Avatar { get; set; }

    } 
}